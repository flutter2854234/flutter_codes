
// Short trick to solve the shift operators
// Left Shift --> Multiply the number with 2 to the power shift bit
// Right Shift --> Divide the number with 2 to the power shift bit.

void main(){
	
	int x = 43;
	print(x << 3);

	int y = 72;
	print(y >> 4);

}

