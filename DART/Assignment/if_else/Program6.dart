// 6. Write a dart program that takes a number from 0 to 5 and prints its spelling .If the number is greater than 5 print entered number is greater than 5.

void main(){

	int num = 10;

	int zero = 0;
	int one = 1;
	int two = 2;
	int three = 3;
	int four = 4;
	int five = 5;

	if(num == 0){
		print("Zero");
	}
	else if(num == 1){
		print("One");
	}
	else if(num == 2){
		print("Two");
	}
	else if(num == 3){
		print("Three");
	}
	else if(num == 4){
		print("Four");
	}
	else if(num == 5){
		print("Five");
	}
	else{
		print("Entered number is greater than 5");
	}
}

