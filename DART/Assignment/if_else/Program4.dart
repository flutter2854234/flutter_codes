
// 4. Write a dart program, take a number and print whether it is positive or negative.


void main(){

	var num = -4;

	if(num > 0){
		print('$num is a positive number');
	}
	else if(num < 0){
		print('$num is negative number');
	}
	else{
		print('$num is equal to zero.');
	}

}




