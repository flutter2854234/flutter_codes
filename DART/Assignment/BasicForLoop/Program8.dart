
// 8. Write a program to print a table of 12 in reverse order

void main(){

	for(int i = 10; i >= 1 ; i--){

		int ans = 12 * i;

		print('12 X $i = $ans');
	}
}
